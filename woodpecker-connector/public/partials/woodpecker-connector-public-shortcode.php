<?php

/**
 * Shortcode view [openimmo_objects] & [openimmo_slider]
 * partial elemnt to send email from page
 *
 * @link       #
 * @since      1.0.0
 *
 * @package    Im_Xml
 * @subpackage Im_Xml/public/partials
 */

if (!defined('woodpecker-shortcode')) {
    die('Direct access not permitted');
}
$options = get_option($this->plugin_name);
?>

<div class="woodpecker-connector-shortcode">
    <style>
        .woodpecker-connector-shortcode .woodpecker-connector-btn{
            color: <?php echo $options['color_submit']; ?>;
            background-color: <?php echo $options['color_submit_bg']; ?>;
        }
        .woodpecker-connector-shortcode .woodpecker-connector-btn:hover,
        .woodpecker-connector-shortcode .woodpecker-connector-btn:active{
            background-color: <?php echo $options['color_submit_bg_hover']; ?>;
        }
    </style>

    <form class="woodpecker-connector-form" action="">
        <input name="<?php echo $this->plugin_name ?>-campaign" type="hidden" value="<?php echo $atts['id']; ?>">

        <?php
        if(!$options['gform_first_hide']){
            ?>
            <div class="woodpecker-connector-group form-group">
                <label><?php _e($options['gform_first'], $this->plugin_name); ?></label>
                <input name="<?php echo $this->plugin_name ?>-first_name" type="text" class="form-control woodpecker-connector-control"  >
            </div>
            <?php
        }
        ?>
        <?php
        if(!$options['gform_last_hide']){
            ?>
            <div class="form-group woodpecker-connector-group">
                <label><?php _e($options['gform_last'], $this->plugin_name); ?></label>
                <input name="<?php echo $this->plugin_name ?>-last_name" type="text" class="form-control woodpecker-connector-control"  >
            </div>
        <?php
        }
        ?>
        <?php
        if(!$options['gform_company_hide']){
            ?>
            <div class="form-group woodpecker-connector-group">
                <label><?php _e($options['gform_company'], $this->plugin_name); ?></label>
                <input name="<?php echo $this->plugin_name ?>-company" type="text" class="form-control woodpecker-connector-control"  >
            </div>
            <?php
        }
        ?>

        <div class="form-group woodpecker-connector-group">
            <label><?php _e($options['gform_email'], $this->plugin_name); ?></label>
            <input name="<?php echo $this->plugin_name ?>-email" type="email" class="form-control woodpecker-connector-control"  >
        </div>

        <div class="form-group woodpecker-connector-group woodpecker-connector-privacy_policy">

            <?php _e($options['privacy_policy'], $this->plugin_name); ?>

        </div>

        <div class="form-group woodpecker-connector-group">
            <button class="btn woodpecker-connector-btn woodpecker-connector-trigger" type="button" class="btn btn-default" data-sub-id="<?php echo $atts['id']; ?>" data-ajaxurl="<?php echo admin_url('admin-ajax.php'); ?>">
                <?php _e($options['gform_submit'], $this->plugin_name); ?>
            </button>
        </div>
    </form>
    <div class="woodpecker-connector-response-container"></div>

</div>
<?php

?>