(function( $ ) {
	'use strict';
	$( document ).ready(function() {
		jQuery(".woodpecker-connector-trigger").on('click', function () {
			event.preventDefault();
			var parameters = [];
			var thisform = jQuery(this);
            jQuery('.woodpecker-connector-response-container').slideUp().html('')
			jQuery(this).parents('div.woodpecker-connector-shortcode').find('input').each(function(){
				var par_name = jQuery(this).attr("name");
				var par_value = jQuery(this).val();
				var item = {}
				item['name'] = par_name;
				item['val'] = par_value;
				parameters.push(item);
				jQuery(this).removeClass('woodpecker-connector-input-danger');
			});
			var parametersString = parameters;
			var data = {
				action: 'add_prospect_to_campaing',
				parameters: parametersString
			};
			console.log(parametersString);
			var ajaxurl = jQuery(this).data('ajaxurl');

			jQuery.post(ajaxurl, data, function (response) {
				console.log('hit');
				//console.log(response);
				if(response == 0){
					jQuery('.woodpecker-connector-response-container').html('Sorry no response');
				}else{
					var data = jQuery.parseJSON(response);
					if(data.action == 'ERROR'){

						jQuery('.woodpecker-connector-response-container').html('<div class="alert alert-danger woodpecker-connector-danger">' + data.message + '</div>').slideDown();
						thisform.parents('div.woodpecker-connector-shortcode').find('input').each(function(){
							jQuery(this).addClass('woodpecker-connector-input-danger');
						});

					}else if(data.action == 'DUPLICATE'){

						jQuery('.woodpecker-connector-response-container').html('<div class="alert alert-warning woodpecker-connector-warning">' + data.message + '</div>').slideDown();
						thisform.parents('div.woodpecker-connector-shortcode').find('input').each(function(){
							jQuery(this).addClass('woodpecker-connector-input-danger');
						});
					}else{

						jQuery('.woodpecker-connector-response-container').html('<div class="alert alert-success woodpecker-connector-success">' + data.message + '</div>').slideDown();
						thisform.parents('div.woodpecker-connector-shortcode').find('input').each(function(){
							jQuery(this).val('')
						});

					}

				}
			}).done(function () {
					console.log("second success");
				})
				.fail(function () {
					console.log("error");
					jQuery('.woodpecker-connector-response-container').html('Error');
				})
				.always(function () {
					console.log("finished");
				});

		});
	});


})( jQuery );


