<?php
/**
 * Created by Wojtek
 * User: Wojciech
 * Conect to woodpecker API
 */
class Woodpecker_Connector_Curl
{
    /**
     * data to API
     *
     * @var string
     */
    private $url = 'https://api.woodpecker.co';
    private $urlconnect = '';

    private $apikey = '';

    private $urlaction = '';

    /**
     * nazwa uzytkownika
     *
     * @var string
     */
    private $username = '';

    /**
     * haslo
     *
     * @var string
     */
    private $password = '';

    /**
     * response data
     *
     * @var string
     */
    private $thisJson = '';

    /**
     * send data
     *
     * @var string
     */
    private $postdata = '';




    /**
     * Constructor
     *
     * @param string $urlaction
     * @param string $apikey
     * @param string $password
     *
     */
    public function __construct($urlaction, $apikey, $postdata = array() ) {

        if($urlaction == '' OR $apikey == '' ){
            return 'Need data to connect';
        }

        //$this->url_xml = trim($url_xml,-2);
        $this->urlconnect = $this->url . preg_replace('/\s+/', '', $urlaction);
        $this->apikey = $apikey;
        $this->password = 'Xuh^7swkOd';
        $this->postdata = $postdata;
    }


    private function curlConnect(){
        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_URL,  $this->urlconnect);
        if( count($this->postdata) ){
            //$jsonData =json_encode($this->postdata, true);
            //var_dump($this->postdata);
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $this->postdata );
        }else{
            curl_setopt( $ch, CURLOPT_POST, false);
        }
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json', 'Cache-Control: no-cache'));
        curl_setopt( $ch, CURLOPT_HEADER, FALSE);
        curl_setopt( $ch, CURLOPT_USERPWD, $this->apikey. ":".$this->password);
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );

        $jsonData = curl_exec($ch);

        $return = "";
        if( curl_error( $ch ) )
        {
            $return .= "Error occurred " . curl_error( $ch ).'<br>';
        }

        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ( $httpCode != 200 )
        {
            $return .=  "Return code is {$httpCode} \n".'<br>'.curl_error($ch);
        }
        else {
            $return .= "<br><pre>".htmlspecialchars( $result )."</pre><br><br>";
        }
        //var_dump($return);
        //var_dump($jsonData);
        curl_close($ch);
        $this->thisJson = $jsonData;
        $this->thisJsonerror = $return;
    }

    public function getJson()
    {
        $this->curlConnect();
        return json_decode($this->thisJson);
    }

}