=== Woodpecker LeadForm ===
Plugin Name: Woodpecker LeadForm Plugin
Contributors: (this should be a list of wordpress.org userid's)
Tags: woodpecker, woodpecker connector, Woodpecker LeadForm, newsletter, subscribe list, Woodpecker forms, sign-up form, opt-in forms, mailing list
Requires at least: 4.9.0
Tested up to: 4.9.0
Requires PHP: 5.6.36
Copyright: (c) 2019
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Add Woodpecker LeadForm to your Wordpress and enjoy automatic transfer of data from the LeadForm into your Woodpecker campaign. Never lose a lead again.

== Description ==

Woodpecker LeadForm is a simple form builder for WordPress.

It connects to your Woodpecker account and transfers the entered lead data into a relevant email outreach campaign.

Add Woodpecker LeadForm to your Wordpress and enjoy automatic transfer of data from the LeadForm into your Woodpecker campaign.

Gather a lead’s name, email address or any other info you want to contact them via Woodpecker.

Stop transferring lead data from one tool to another. Use a plugin that automates that for you.

What is Woodpecker? Woodpecker is an email outreach and follow-up automation tool. Sign up here to start contacting your website visitors today.

What will Woodpecker do for you?
Start more conversations.
Nurture relationships.
Automate first-touch messages.


Why would you use this lead form?
Never lose a lead again.
Contact only the interested ones.
Let your leads decide to leave their data with you.

Note: The plugin doesn't store any data. It immediately transfers data into the Woodpecker account you connect it with. Please get familiar with the Woodpecker Privacy Policy (link) before using the Plugin.


= Plugin Features: =
* You can list campaign and generate subscribe forms.
* You can list prospects.
* Never lose a lead again.
* Contact only the interested ones.
* Let your leads decide to leave their data with you.


== Installation ==

1. Upload `woodpecker-connector` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Add API and Password in setting
4. Ready to GO!

== Frequently Asked Questions ==

Visit our Help section to learn more about the Plugin.

= Do I need a Woodpecker account to use Woodpecker LeadForm? =

You do, because Woodpecker LeadForm sends the lead data into Woodpecker. It won’t work properly without being connected to an account at Woodpecker.

= How do I get a Woodpecker account? =

Sign up at app.woodpecker.co/signup

= How does the Plugin connect to my Woodpecker account? =

It connects via API key generated in Woodpecker and then pasted into the Plugin.

= How do I make my forms GDPR compliant? =

Include a statement below the form.

= How many lead forms can I have? =

As many as you need.

= How do I create a Woodpecker campaign for people who left their contact info? =

Choose the contacts from the Prospect list and add them to a Woodpecker campaign.

== Screenshots ==

1. Woodpecker Connector

== Upgrade Notice ==

Automatic updates should work great for you.  As always, though, we recommend backing up your site prior to making any updates just to be sure nothing goes wrong.

== Changelog ==

= 1.0 =
General settings
List camaign and shortcodes
List prospects
Custom style for form