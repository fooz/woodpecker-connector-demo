(function ($) {
    'use strict';

    $(function() {
        $('.color-field').wpColorPicker();
    });


    jQuery( document ).ready(function(){

        jQuery('#woodpecker-options .woodpecker-help-trigger').on('click', function(){
                jQuery(this).toggleClass('opened');
                jQuery('#woodpecker-options .woodpecker-help').slideToggle();
        });


    });



})(jQuery);

function showHideApi(){
    var x = jQuery('.api_key_input').prop('type');
    console.log(x);
    if (x === "password") {
        jQuery('.api_key_input').prop('type', 'text');
    } else {
        jQuery('.api_key_input').prop('type', 'password');
    }
}