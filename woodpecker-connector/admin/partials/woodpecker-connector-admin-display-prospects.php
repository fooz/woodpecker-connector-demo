<?php
/**
 * Admin backend for How Its Works
 * partial elemnt
 *
 * @link       #
 * @since      1.0.0
 *
 * @package    Woodpecker_Connector
 * @subpackage Woodpecker_Connector/admin/partials
 */
if (!defined('Woodpecker_Connector_Admin')) {
    die('Direct access not permitted');
}

?>
    <div class="col-container">
        <?php
        $getconnectprospects = new Woodpecker_Connector_Curl('/rest/v1/prospects?status=ACTIVE,TO-CHECK,TO-REVIEW,REPLIED,AUTOREPLIED&per_page=100&campaigns_details=true',
            $options['api_key'] );
        // all status
        // ACTIVE  | TO-CHECK | TO-REVIEW | REPLIED | AUTOREPLIED | PAUSED | BLACKLIST|INVALID | BOUNCE
        $getjsonprospects = $getconnectprospects->getJson();
        //var_dump(json_decode($getjsonprospects));
        $getstatus = $getjsonprospects->status;
        if($getstatus->status == 'ERROR'){
            ?>
                <div class="notice notice-error">
                    <br>
                    <?php _e('We weren\'t able to connect to Woodpecker. The API key is incorrect or no longer valid. Check your API key and try again. You can generate a new key in your Woodpecker account in "Settings".', $this->plugin_name); ?>
                    <br><br>
                    <?php echo $getstatus->msg; ?>
                    <br><br>
                </div>
            <?php
        }else if($options['api_key'] == ''){
            ?>
                <div class="notice notice-error">
                    <br>
                    <?php _e('You need to generate API key. Go to "Settings" in your Woodpecker account', $this->plugin_name); ?>
                    <br><br>
                </div>
            <div class="col-row">
                <div class="col-left">
                </div>
                <div class="col-right">
                    <p>
                        <?php _e("Looks like you haven't connect plugin to Woodpecker.", $this->plugin_name); ?><br>
                        <a href="?page=woodpecker-connector&tab=settings">Go to settings and provide api key.</a>
                    </p>
                </div>
            </div>
            <?php
        }else{

            ?>
            <table class="form-table widefat">
                <thead>
                <tr>
                    <th><?php _e('Email', $this->plugin_name); ?></th>
                    <th><?php _e('First name', $this->plugin_name); ?></th>
                    <th><?php _e('Last name', $this->plugin_name); ?></th>
                    <th><?php _e('Company', $this->plugin_name); ?></th>
                    <th><?php _e('Status', $this->plugin_name); ?></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <?php
                    foreach ((array)$getjsonprospects as $prosp) {
                    ?>
                <tr>
                    <th><?php echo $prosp->email; ?></th>
                    <th><?php echo $prosp->first_name; ?></th>
                    <th><?php echo $prosp->last_name; ?></th>
                    <th><?php echo $prosp->company; ?></th>
                    <th><?php echo $prosp->status; ?></th>
                </tr>
                <?php
                }
                ?>
                </tbody>
            </table>
            <?php

            /*
            foreach ((array)$getjsonprospects as $prosp) {
                echo 'id: ' . $prosp->id . '<br>';
                echo 'first name: ' . $prosp->first_name . '<br>';
                echo 'last name: ' . $prosp->last_name . '<br>';
                echo 'email: ' . $prosp->email . '<br>';
                echo 'company: ' . $prosp->company . '<br>';
                echo 'status: ' . $prosp->status . '<br>';

                foreach ((array)$prosp->campaigns_details as $prospcamp) {
                    echo 'Campaingn <br>';
                    echo 'id: ' . $prospcamp->campaign_id . '<br>';
                    echo 'name: ' . $prospcamp->campaign_name . '<br>';
                    echo 'status: ' . $prospcamp->campaign_status . '<br>';
                    echo 'prospect status: ' . $prospcamp->campaign_prospect_status . '<br>';
                    echo '<br><hr><br>';
                }
                echo '<br><hr><br>';
            }*/

        }
        ?>
    </div>
<?php

?>