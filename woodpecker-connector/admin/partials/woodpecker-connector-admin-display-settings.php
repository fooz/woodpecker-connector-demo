<?php
/**
 * Admin backend for How Its Works
 * partial elemnt
 *
 * @link       #
 * @since      1.0.0
 *
 * @package    Woodpecker_Connector
 * @subpackage Woodpecker_Connector/admin/partials
 */
if (!defined('Woodpecker_Connector_Admin')) {
    die('Direct access not permitted');
}
?>

<form method="post" name="woodpecker-options" action="options.php" enctype="multipart/form-data">

    <div class="col-container">
        <?php
        settings_fields($this->plugin_name);
        do_settings_sections($this->plugin_name);

        $getconnectcampaign = new Woodpecker_Connector_Curl('/rest/v1/campaign_list', $options['api_key']);
        $getjsoncampaign = $getconnectcampaign->getJson();
        //var_dump($getjsoncampaign);
        $getstatus = $getjsoncampaign->status;
        if ($getstatus->status == 'ERROR') {
            ?>

            <div class="notice notice-error">
                <br>
                <?php _e('The API key is incorrect or no longer valid. Check your API key and try again. You can generate a new key in your Woodpecker account in "Settings". ', $this->plugin_name); ?>
                <br><br>
                <?php echo $getstatus->msg; ?>
                <br><br>
            </div><br><br>

            <?php
        } else if ($options['api_key'] == '') {
            ?>

            <div class="notice notice-error">
                <br>
                <?php _e('You need to generate API key. Go to "Settings" in your Woodpecker account', $this->plugin_name); ?>
                <br><br>
            </div><br><br>

            <?php
        }
        ?>
        <div class="col-row">
            <div class="col-left">
                <div class="col-wrap">
                    <h2><?php _e('Woodpecker API key', $this->plugin_name); ?></h2>
                </div>
            </div>
            <div class="col-right">
                <div class="col-wrap">
                    <fieldset>
                        <label><?php esc_attr_e('API key (generate in the Woodpecker Settings)', $this->plugin_name); ?></label>
                        <input type="password"
                               class="large-text api_key_input"
                               id="<?php echo $this->plugin_name; ?>-api_key"
                               name="<?php echo $this->plugin_name; ?>[api_key]"
                               value="<?php echo $options['api_key']; ?>"/>
                        <span id="show-api" class="show-api dashicons dashicons-visibility"
                              onclick="showHideApi()"></span>
                    </fieldset>
                    <fieldset>
                        <?php submit_button(__('Save changes', $this->plugin_name), 'primary', 'submit', TRUE); ?>
                    </fieldset>
                </div>
            </div>
        </div>
        <br>
        <br>
        <div class="col-row">
            <div class="col-left">
                <div class="col-wrap">
                    <h2><?php _e('Privacy policy', $this->plugin_name); ?></h2>
                </div>
            </div>
            <div class="col-right">
                <div class="col-wrap">
                    <fieldset>
                        <label><?php esc_attr_e('Privacy policy text (you can use html to add link)', $this->plugin_name); ?></label>
                        <textarea
                            rows="5"
                            class="regular-text"
                            id="<?php echo $this->plugin_name; ?>-privacy_policy"
                            name="<?php echo $this->plugin_name; ?>[privacy_policy]"
                        ><?php echo $options['privacy_policy']; ?></textarea>
                    </fieldset>
                    <fieldset>
                        <?php submit_button(__('Save changes', $this->plugin_name), 'primary', 'submit', TRUE); ?>
                    </fieldset>
                </div>
            </div>
        </div>
        <br>
        <br>
        <div class="col-row">
            <div class="col-left">
                <div class="col-wrap">
                    <h2><?php _e('Global forms options', $this->plugin_name); ?></h2>
                </div>
            </div>
            <div class="col-right">
                <div class="col-wrap">
                    <fieldset>
                        <label><?php esc_attr_e('Email label', $this->plugin_name); ?></label>
                        <input type="text"
                               class="regular-text"
                               id="<?php echo $this->plugin_name; ?>-gform_email"
                               name="<?php echo $this->plugin_name; ?>[gform_email]"
                               value="<?php echo $options['gform_email']; ?>"/>
                    </fieldset>
                    <fieldset>
                        <label><?php esc_attr_e('Form first name label', $this->plugin_name); ?></label>
                        <input type="text"
                               class="regular-text"
                               id="<?php echo $this->plugin_name; ?>-gform_first"
                               name="<?php echo $this->plugin_name; ?>[gform_first]"
                               value="<?php echo $options['gform_first']; ?>"/>
                        <input type="checkbox"
                               class="show-input-checkbox"
                               id="<?php echo $this->plugin_name; ?>-gform_first_hide"
                               name="<?php echo $this->plugin_name; ?>[gform_first_hide]"
                               value="1" <?php checked($options['gform_first_hide'], 1); ?> /> Hide this field
                    </fieldset>
                    <fieldset>
                        <label><?php esc_attr_e('Last first name label', $this->plugin_name); ?></label>
                        <input type="text"
                               class="regular-text"
                               id="<?php echo $this->plugin_name; ?>-gform_last"
                               name="<?php echo $this->plugin_name; ?>[gform_last]"
                               value="<?php echo $options['gform_last']; ?>"/>
                        <input type="checkbox"
                               class="show-input-checkbox"
                               id="<?php echo $this->plugin_name; ?>-gform_last_hide"
                               name="<?php echo $this->plugin_name; ?>[gform_last_hide]"
                               value="1" <?php checked($options['gform_last_hide'], 1); ?> /> Hide this field
                    </fieldset>
                    <fieldset>
                        <label><?php esc_attr_e('Company name label', $this->plugin_name); ?></label>
                        <input type="text"
                               class="regular-text"
                               id="<?php echo $this->plugin_name; ?>-gform_company"
                               name="<?php echo $this->plugin_name; ?>[gform_company]"
                               value="<?php echo $options['gform_company']; ?>"/>
                        <input type="checkbox"
                               class="show-input-checkbox"
                               id="<?php echo $this->plugin_name; ?>-gform_company_hide"
                               name="<?php echo $this->plugin_name; ?>[gform_company_hide]"
                               value="1" <?php checked($options['gform_company_hide'], 1); ?> /> Hide this field
                    </fieldset>
                    <fieldset>
                        <label><?php esc_attr_e('Submit button label', $this->plugin_name); ?></label>
                        <input type="text"
                               class="regular-text"
                               id="<?php echo $this->plugin_name; ?>-gform_submit"
                               name="<?php echo $this->plugin_name; ?>[gform_submit]"
                               value="<?php echo $options['gform_submit']; ?>"/>
                    </fieldset>

                    <fieldset>
                        <?php submit_button(__('Save changes', $this->plugin_name), 'primary', 'submit', TRUE); ?>
                    </fieldset>

                    <br>
                    <br>

                    <fieldset>
                        <label><?php esc_attr_e('Success message', $this->plugin_name); ?></label>
                        <textarea
                            rows="5"
                            class="regular-text"
                            id="<?php echo $this->plugin_name; ?>-message_success"
                            name="<?php echo $this->plugin_name; ?>[message_success]"
                            ><?php echo $options['message_success']; ?></textarea>
                    </fieldset>
                    <fieldset>
                        <label><?php esc_attr_e('Error message', $this->plugin_name); ?></label>
                        <textarea
                            rows="5"
                            class="regular-text"
                            id="<?php echo $this->plugin_name; ?>-message_error"
                            name="<?php echo $this->plugin_name; ?>[message_error]"
                            ><?php echo $options['message_error']; ?></textarea>
                    </fieldset>
                    <fieldset>
                        <label><?php esc_attr_e('Already exists message', $this->plugin_name); ?></label>
                        <textarea
                            rows="5"
                            class="regular-text"
                            id="<?php echo $this->plugin_name; ?>-message_exist"
                            name="<?php echo $this->plugin_name; ?>[message_exist]"
                            ><?php echo $options['message_exist']; ?></textarea>
                    </fieldset>

                    <fieldset>
                        <?php submit_button(__('Save changes', $this->plugin_name), 'primary', 'submit', TRUE); ?>
                    </fieldset>
                </div>
            </div>
        </div>
        <br>
        <br>
        <div class="col-row">
            <div class="col-left">
                <div class="col-wrap">
                    <h2><?php _e('Forms style settings', $this->plugin_name); ?></h2>
                </div>
            </div>
            <div class="col-right">
                <div class="col-wrap">

                    <fieldset >
                        <?php esc_attr_e('Form sumbit color', $this->plugin_name);?><br>
                        <label for="<?php echo $this->plugin_name;?>-color_submit">
                            <input type="text"
                                   class="<?php echo $this->plugin_name;?>-color-picker color-field"
                                   id="<?php echo $this->plugin_name;?>-color_submit"
                                   name="<?php echo $this->plugin_name;?>[color_submit]"
                                   value="<?php echo $options['color_submit']; ?>" />
                        </label>
                    </fieldset>
                    <fieldset >
                        <?php esc_attr_e('Form submit background color', $this->plugin_name);?><br>
                        <label for="<?php echo $this->plugin_name;?>-color_submit_bg">
                            <input type="text"
                                   class="<?php echo $this->plugin_name;?>-color-picker color-field"
                                   id="<?php echo $this->plugin_name;?>-color_submit_bg"
                                   name="<?php echo $this->plugin_name;?>[color_submit_bg]"
                                   value="<?php echo $options['color_submit_bg']; ?>" />
                        </label>
                    </fieldset>
                    <fieldset >
                        <?php esc_attr_e('Form submit hover background color', $this->plugin_name);?><br>
                        <label for="<?php echo $this->plugin_name;?>-color_submit_bg_hover">
                            <input type="text"
                                   class="<?php echo $this->plugin_name;?>-color-picker color-field"
                                   id="<?php echo $this->plugin_name;?>-color_submit_b_-hover"
                                   name="<?php echo $this->plugin_name;?>[color_submit_bg_hover]"
                                   value="<?php echo $options['color_submit_bg_hover']; ?>" />
                        </label>
                    </fieldset>
                    <?php /*
                    <p>
                        Some styles in next update<br>
                        - form width<br>
                        - input color<br>
                        - border width<br>
                        - border color<br>
                        x button color<br>
                        x button background color<br>
                        x button hover color<br>
                        - border radius<br>
                    </p>
                    */ ?>
                    <fieldset>
                        <?php submit_button(__('Save changes', $this->plugin_name), 'primary', 'submit', TRUE); ?>
                    </fieldset>

                </div>
            </div>
        </div>
    </div>
</form>
<?php

?>