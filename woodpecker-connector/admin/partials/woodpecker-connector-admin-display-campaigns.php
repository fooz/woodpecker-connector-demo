<?php
/**
 * Admin backend for How Its Works
 * partial elemnt
 *
 * @link       #
 * @since      1.0.0
 *
 * @package    Woodpecker_Connector
 * @subpackage Woodpecker_Connector/admin/partials
 */
if (!defined('Woodpecker_Connector_Admin')) {
    die('Direct access not permitted');
}

?>
    <div class="col-container">

        <?php
        $getconnectcampaign = new Woodpecker_Connector_Curl('/rest/v1/campaign_list', $options['api_key']);
        $getjsoncampaign = $getconnectcampaign->getJson();
        //var_dump($getjsoncampaign);
        $getstatus = $getjsoncampaign->status;
        if ($getstatus->status == 'ERROR') {
            ?>

                <div class="notice notice-error">
                    <br>
                    <?php _e('We weren\'t able to connect to Woodpecker. The API key is incorrect or no longer valid. Check your API key and try again. You can generate a new key in your Woodpecker account in "Settings".', $this->plugin_name); ?>
                    <br><br>
                    <?php echo $getstatus->msg; ?>
                    <br><br>
                </div>

            <?php
        } else if ($options['api_key'] == '') {
            ?>

                <div class="notice notice-error">
                    <br>
                    <?php _e('You need to generate API key. Go to "Settings" in your Woodpecker account', $this->plugin_name); ?>
                    <br><br>
                </div>

            <div class="col-row">
                <div class="col-left">
                </div>
                <div class="col-right">
                    <p>
                        <?php _e("Looks like you haven't connect plugin to Woodpecker.", $this->plugin_name); ?><br>
                        <a href="?page=woodpecker-connector&tab=settings">Go to settings and provide api key.</a>
                    </p>
                </div>
            </div>
            <?php
        } else {
            ?>
            <p>
                <?php _e('If you want to send prospects to a general prospect list, use this shortcode:', $this->plugin_name); ?><br><br>
                <?php echo '[' . $this->plugin_name . ']' ?> <br><br>
            </p>
            <p>
                <?php _e('If you want to send prospects to a particular campaign, copy the shortcode from the first column.', $this->plugin_name); ?>
            </p>
            <table class="form-table widefat">
                <thead>
                <tr>
                    <th><?php _e('Get&nbsp;shortcode (copy text to page)', $this->plugin_name); ?></th>
                    <th><?php _e('Name', $this->plugin_name); ?></th>
                    <th><?php _e('Status', $this->plugin_name); ?></th>
                    <th><?php _e('From', $this->plugin_name); ?></th>
                    <th><?php _e('Created', $this->plugin_name); ?></th>
                    <th>ID</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <?php
                    foreach ((array)$getjsoncampaign as $camp) {
                    ?>
                <tr>
                    <td><?php /*
                                <button class="table-button" data-id="<?php echo $camp->id; ?>"><?php _e('Get&nbsp;shortcode', $this->plugin_name); ?></button>*/ ?>
                        <?php echo '[' . $this->plugin_name . ' id=' . $camp->id . ']' ?>
                    </td>
                    <td><?php echo $camp->name; ?></td>
                    <td><?php echo $camp->status; ?></td>
                    <td><?php echo $camp->from_name . '<br>' . $camp->from_email; ?></td>
                    <td><?php echo date('d.m.Y H:s', strtotime($camp->created));?></td>
                    <td><?php echo $camp->id; ?></td>
                </tr>
                <?php
                }
                ?>
                </tbody>
            </table>
            <?php
        }
        ?>

    </div>
<?php

?>