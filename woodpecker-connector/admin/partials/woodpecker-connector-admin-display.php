<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       #
 * @since      1.0.0
 *
 * @package    Woodpecker_Connector
 * @subpackage Woodpecker_Connector/admin/partials
 */
$active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'howitworks';
?>
<div id="woodpecker-options" class="woodpecker-options-wrap">


    <?php
    define('Woodpecker_Connector_Admin', TRUE); // secure files
    ?>
    <div class="woodpecker-header">
        <img src="<?php echo $this->url . 'admin/images/app-logo.svg'; ?>" alt="Woodpecker">
        <div class="woodpecker-header-content">
            <h1>Woodpecker LeadForm Plugin</h1>
            <div  class="nav-tab-wrapper">
                <a href="?page=woodpecker-connector&tab=howitworks"
                   class="nav-tab <?php echo $active_tab == 'howitworks' ? 'nav-tab-active' : ''; ?>"><?php _e('How it works', $this->plugin_name); ?></a>
                <a href="?page=woodpecker-connector&tab=campaigns"
                   class="nav-tab <?php echo $active_tab == 'campaigns' ? 'nav-tab-active' : ''; ?>"><?php _e('Campaigns', $this->plugin_name); ?></a>
                <a href="?page=woodpecker-connector&tab=prospects"
                   class="nav-tab <?php echo $active_tab == 'prospects' ? 'nav-tab-active' : ''; ?>"><?php _e('Prospects', $this->plugin_name); ?></a>
                <a href="?page=woodpecker-connector&tab=settings"
                   class="nav-tab <?php echo $active_tab == 'settings' ? 'nav-tab-active' : ''; ?>"><?php _e('Settings', $this->plugin_name); ?></a>
            </div>
        </div>
    </div>



    <?php
    settings_fields($this->plugin_name);
    //var_dump($_POST);
    //Grab all options
    $options = get_option($this->plugin_name);
    //var_dump($options);
    ?>

    <?php
    // HELP LEAD
    if ($active_tab == '' || $active_tab == 'howitworks') {
        include_once( 'woodpecker-connector-admin-display-how-it-works.php' );
    }

    ?>
    <?php
    // CAMPAIGNS OPTIONS
    if ($active_tab == 'campaigns') {
        include_once( 'woodpecker-connector-admin-display-campaigns.php' );
    }
    ?>
    <?php
    // PROSPECTS OPTIONS
    if ($active_tab == 'prospects') {
        include_once( 'woodpecker-connector-admin-display-prospects.php' );
    }
    ?>
    <?php
    // GENERAL SETTINGS OPTIONS
    if ($active_tab == 'settings') {
        include_once( 'woodpecker-connector-admin-display-settings.php' );
    }
    ?>

</div>
