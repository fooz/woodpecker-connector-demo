<?php
/**
 * Admin backend for How Its Works
 * partial elemnt
 *
 * @link       #
 * @since      1.0.0
 *
 * @package    Woodpecker_Connector
 * @subpackage Woodpecker_Connector/admin/partials
 */
if (!defined('Woodpecker_Connector_Admin')) {
    die('Direct access not permitted');
}
?>
<div class="col-container">
    <div class="col-row">
        <div class="col-left">
            <div class="col-wrap">
                <h2>What we do?</h2>
            </div>
        </div>
        <div class="col-right">
            <div class="col-wrap">
                <p>
                    <?php _e('Woodpecker LeadForm Plugin allows you to integrate a Woodpecker account with your WordPress website. ', $this->plugin_name); ?>
                    <br>
                    <?php _e('You can use it to send contact information of the prospects who filled out the lead form.', $this->plugin_name); ?>
                    <br><br>
                    <?php _e('To use it, you need a Woodpecker account and an API key that you can generate in your Woodpecker account.', $this->plugin_name); ?>
                    <br><br>
                    <?php _e('To generate a new API Key, follow the steps below.', $this->plugin_name); ?>
                </p>
            </div>
        </div>
    </div>
    <br>
    <br>
    <div class="col-row">
        <div class="col-left">
            <div class="col-wrap">
                <h2>Help</h2>
            </div>
        </div>
        <div class="col-right">
            <div class="col-wrap">
                <div class="woodpecker-help">
                    <strong><?php _e('Step 1', $this->plugin_name); ?></strong>
                    <?php _e('Log into your Woodpecker account at ', $this->plugin_name); ?>
                    <a href="" target="_blank">app.woodpecker.co</a><br>
                    <hr>
                    <strong><?php _e('Step 2', $this->plugin_name); ?></strong>
                    <?php _e('Go to "Settings" > "API Keys" ', $this->plugin_name); ?>
                    <br><br>
                    <img class="img-responsive" src="<?php echo $this->url . 'admin/images/go-to-settings.png'; ?>"
                         alt=""><br>
                    <hr>
                    <strong><?php _e('Step 3', $this->plugin_name); ?></strong>
                    <?php _e('Click "Create a Key" ', $this->plugin_name); ?>
                    <br><br>
                    <img class="img-responsive" src="<?php echo $this->url . 'admin/images/go-to-api-key-create-key.png'; ?>"
                         alt=""><br><br>
                    <hr>
                    <strong>
                        <?php _e('Step 4', $this->plugin_name); ?></strong>
                        <?php _e('Now that you generated your API key, go to "Settings" in Woodpecker LeadForm Plugin and paste it in "Woodpecker API". ', $this->plugin_name); ?>
                    </strong><br><br>
                    <img class="img-responsive" src="<?php echo $this->url . 'admin/images/woodpecker-plugin-settings.png'; ?>"
                         alt=""><br><br>
                    <hr>
                    <?php _e('<br><br>
                        Please enter the Campaign Tab in your Woodpecker LeadForm Plugin. In the Campaign Section, all your Campaigns from your Woodpecker App will be listed.  Choose the Campaign, that will be the key Campaign for your LeadForm Plugin and copy the short code that will appear on the left side of the table.
                        <br><br>
                        If you want to add the form onto your Wordpress page or post, paste the short code into the editor. All the prospects from the Woodpecker Plugin will drop into this particular Campaign.', $this->plugin_name); ?>
                    <br><br>
                </div>
            </div>
        </div>
    </div>
</div>
